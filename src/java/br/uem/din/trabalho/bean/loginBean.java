package br.uem.din.trabalho.bean;

import br.uem.din.trabalho.controller.ProjetoController;
import br.uem.din.trabalho.controller.UsuarioController;
import br.uem.din.trabalho.model.Beneficiario;
import br.uem.din.trabalho.model.Participante;
import br.uem.din.trabalho.model.Pessoa;
import br.uem.din.trabalho.model.Programacao;
import br.uem.din.trabalho.model.Projeto;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "loginBean")
@SessionScoped
public class loginBean implements Serializable {

    private String login;
    private String senha;

    public loginBean() {
    }

    public Pessoa getSessaoUsuario() {
        return UsuarioController.getInstance().mostrarUsuario();
    }

    public String autenticar() {
        preencherProjetos();
        String email = getLogin() + "@uem.br";
        Pessoa pessoa = new Pessoa("", "", getLogin(), email, "", BigDecimal.ZERO, null);
        UsuarioController.getInstance().salvarUsuario(pessoa);
        return "menu";
    }

    private void preencherProjetos() {
        Programacao prog1 = new Programacao("Teste1", 12);
        Programacao prog2 = new Programacao("Teste2", 43);
        Programacao prog3 = new Programacao("Teste3", 35);
        Programacao prog4 = new Programacao("Teste4", 75);
        Programacao prog5 = new Programacao("Teste5", 25);
        Programacao prog6 = new Programacao("Teste6", 52);
        List<Programacao> lista1 = new ArrayList<>();
        List<Programacao> lista2 = new ArrayList<>();
        List<Programacao> lista3 = new ArrayList<>();

        List<Participante> participante1 = new ArrayList<>();
        List<Participante> participante2 = new ArrayList<>();
        List<Participante> participante3 = new ArrayList<>();

        List<Beneficiario> beneficiario1 = new ArrayList<>();
        List<Beneficiario> beneficiario2 = new ArrayList<>();
        List<Beneficiario> beneficiario3 = new ArrayList<>();

        lista1.add(prog1);
        lista1.add(prog3);
        lista1.add(prog5);

        lista2.add(prog6);
        lista2.add(prog4);
        lista2.add(prog2);

        lista3.add(prog2);
        lista3.add(prog1);
        lista3.add(prog5);

        Projeto projeto1 = new Projeto("Curso de iniciação cientifica", "5682", "2018", lista1, participante1, beneficiario1);
        Projeto projeto2 = new Projeto("Evento de solidificação da Água", "4724", "2019", lista2, participante2, beneficiario2);
        Projeto projeto3 = new Projeto("Curso para Realizar Macarrão Instantâneo", "7235", "2019", lista3, participante3, beneficiario3);
        ProjetoController.getInstance().salvarProjeto(projeto1);
        ProjetoController.getInstance().salvarProjeto(projeto2);
        ProjetoController.getInstance().salvarProjeto(projeto3);
    }

    /*GETS & SETS*/
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
