package br.uem.din.trabalho.bean;

import br.uem.din.trabalho.controller.ParticipanteController;
import br.uem.din.trabalho.controller.ProjetoController;
import br.uem.din.trabalho.model.Participante;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.bean.ManagedBean;
import org.primefaces.component.dialog.Dialog;

@ManagedBean(name = "participanteBean")
@SessionScoped
public class ParticipanteBean implements Serializable {

    private String nome;
    private String cpf;
    private String matricula;
    private String email;
    private String internoUEM;
    private BigDecimal cargaHoraria;
    private String curso;
    private String tipoAtuacao;
    private Dialog alerta1;
    private Dialog alerta2;

    public ParticipanteBean() {
    }

    /*CADASTRO*/
    public String cadastrar() {
        verificarExt();
        if (this.cpf.equals("") || this.cpf == null) {
            this.alerta2.setVisible(true);
            return "participante";
        }
        if (!ParticipanteController.getInstance().verificarParticipante(this.cpf)) {
            this.alerta1.setVisible(false);
            ParticipanteController.getInstance().salvarParticipante(new Participante(curso, tipoAtuacao, nome, cpf, matricula, email, internoUEM, cargaHoraria, ProjetoController.getInstance().listarProjetoSelecionado().get(0)));
            return "participante";
        } else {
            this.alerta1.setVisible(true);
            return "participante";
        }
    }

    public void verificarExt() {
        if (this.getInternoUEM().equals("Comunidade Externa")) {
            this.setCurso("Não possui");
            this.setMatricula("Não possui");
        }
    }

    /*DELETE*/
    public String deletar(String cpf) {
        ParticipanteController.getInstance().deletarParticipante(cpf);
        return "participante";
    }

    public String deletarLista(String cpf) {
        ParticipanteController.getInstance().deletarParticipante(cpf);
        return "lista";
    }

    /*LISTAGEM*/
    public List<Participante> getParticipantes() {
        return ParticipanteController.getInstance().listarPaticipantes();
    }

    /*DEFINIR CARGA HORÁRIA*/
//    public void somaCargaHoraria(int cargaHorariaTema) {
//        if (this.checkboxTema.isSelected()) {
//            this.setCargaHoraria(this.getCargaHoraria() + cargaHorariaTema);
//        } else {
//            this.setCargaHoraria(this.getCargaHoraria() - cargaHorariaTema);
//        }
//    }

    /* GET & SETS*/
    public String getNome() {
        return nome;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getTipoAtuacao() {
        return tipoAtuacao;
    }

    public void setTipoAtuacao(String tipoAtuacao) {
        this.tipoAtuacao = tipoAtuacao;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInternoUEM() {
        return internoUEM;
    }

    public void setInternoUEM(String internoUEM) {
        this.internoUEM = internoUEM;
    }

    public Dialog getAlerta1() {
        return alerta1;
    }

    public void setAlerta1(Dialog alerta1) {
        this.alerta1 = alerta1;
    }

    public Dialog getAlerta2() {
        return alerta2;
    }

    public void setAlerta2(Dialog alerta2) {
        this.alerta2 = alerta2;
    }

    public BigDecimal getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(BigDecimal cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }
}
