package br.uem.din.trabalho.bean;

import br.uem.din.trabalho.controller.ProjetoController;
import br.uem.din.trabalho.model.Projeto;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "menuBean")
@SessionScoped
public class MenuBean implements Serializable {

    private String ano;
    private String nuProcesso;

    public MenuBean() {
    }

    public String hrefMenu() {
        ProjetoController.getInstance().limparProjetoSelecionado();
        return "menu";
    }

    public String hrefLista() {
        return "lista";
    }

    public String hrefLogin() {
        ProjetoController.getInstance().limparProjetoSelecionado();
        return "login";
    }

    public String hrefMenuProjeto() {
        Projeto projeto = ProjetoController.getInstance().getProjeto(this.ano, this.nuProcesso);
        if (projeto != null) {
            ProjetoController.getInstance().projetoSelecionado(this.ano, this.nuProcesso);
            return "menuProjeto";
        }
        return "menu";
    }

    public String hrefParticipante() {
        return "participante";
    }

    public String hrefBeneficiario() {
        return "beneficiario";
    }

    public String cadastrarParticipante(String nuProcesso, String ano) {
        Projeto projeto = ProjetoController.getInstance().getProjeto(ano, nuProcesso);
        if (projeto != null) {
            ProjetoController.getInstance().projetoSelecionado(ano, nuProcesso);
            return "participante";
        }
        return "menu";
    }

    public String cadastrarBeneficiario(String nuProcesso, String ano) {
        Projeto projeto = ProjetoController.getInstance().getProjeto(ano, nuProcesso);
        if (projeto != null) {
            ProjetoController.getInstance().projetoSelecionado(ano, nuProcesso);
            return "beneficiario";
        }
        return "menu";
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getNuProcesso() {
        return nuProcesso;
    }

    public void setNuProcesso(String nuProcesso) {
        this.nuProcesso = nuProcesso;
    }
}
