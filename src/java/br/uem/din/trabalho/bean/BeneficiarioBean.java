package br.uem.din.trabalho.bean;

import br.uem.din.trabalho.controller.BeneficiarioController;
import br.uem.din.trabalho.controller.ProjetoController;
import br.uem.din.trabalho.model.Beneficiario;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.faces.bean.ManagedBean;
import org.primefaces.component.dialog.Dialog;

@ManagedBean(name = "beneficiarioBean")
@SessionScoped
public class BeneficiarioBean implements Serializable {

    private String nome;
    private String cpf;
    private String matricula;
    private String email;
    private String internoUEM;
    private BigDecimal cargaHoraria;
    private BigDecimal frequencia;
    private BigDecimal nota;
    private Dialog alerta1;
    private Dialog alerta2;

    public BeneficiarioBean() {
    }

    /*CADASTRO*/
    public String cadastrar() {
        verificarExt();
        if (this.cpf.equals("") || this.cpf == null) {
            this.alerta2.setVisible(true);
            return "beneficiario";
        }
        if (!BeneficiarioController.getInstance().validarBeneficiario(this.cpf)) {
            this.alerta1.setVisible(false);
            BeneficiarioController.getInstance().salvarBeneficiario(new Beneficiario(frequencia, nota, nome, cpf, matricula, email, internoUEM, cargaHoraria, ProjetoController.getInstance().listarProjetoSelecionado().get(0)));
            return "beneficiario";
        } else {
            this.alerta1.setVisible(true);
            return "beneficiario";
        }
    }

    public void verificarExt() {
        if (this.getInternoUEM().equals("Comunidade Externa")) {
            this.setMatricula("Não possui");
        }
    }

    /*DELETE*/
    public String deletar(String cpf) {
        BeneficiarioController.getInstance().deletarBeneficiario(cpf);
        return "beneficiario";
    }

    public String deletarLista(String cpf) {
        BeneficiarioController.getInstance().deletarBeneficiario(cpf);
        return "lista";
    }

    /*LISTAGEM*/
    public List<Beneficiario> getBeneficiarios() {
        return BeneficiarioController.getInstance().listarBeneficiarios();
    }

    /*GET & SETS*/
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInternoUEM() {
        return internoUEM;
    }

    public void setInternoUEM(String internoUEM) {
        this.internoUEM = internoUEM;
    }

    public Dialog getAlerta1() {
        return alerta1;
    }

    public void setAlerta1(Dialog alerta1) {
        this.alerta1 = alerta1;
    }

    public Dialog getAlerta2() {
        return alerta2;
    }

    public void setAlerta2(Dialog alerta2) {
        this.alerta2 = alerta2;
    }

    public BigDecimal getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(BigDecimal cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public BigDecimal getFrequencia() {
        return frequencia;
    }

    public void setFrequencia(BigDecimal frequencia) {
        this.frequencia = frequencia;
    }

    public BigDecimal getNota() {
        return nota;
    }

    public void setNota(BigDecimal nota) {
        this.nota = nota;
    }

}
