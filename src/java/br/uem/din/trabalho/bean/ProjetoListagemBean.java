package br.uem.din.trabalho.bean;

import br.uem.din.trabalho.controller.ProjetoController;
import br.uem.din.trabalho.model.Programacao;
import br.uem.din.trabalho.model.Projeto;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;

@ManagedBean(name = "projetoListagemBean")
@SessionScoped
public class ProjetoListagemBean implements Serializable {

    public ProjetoListagemBean() {
    }

    public List<Projeto> getProjetos() {
        return ProjetoController.getInstance().listarProjetos();
    }

    public Projeto getProjeto(String ano, String nuProcesso) {
        return ProjetoController.getInstance().getProjeto(ano, nuProcesso);
    }

    public List<Projeto> getProjetoSelecionado() {
        return ProjetoController.getInstance().listarProjetoSelecionado();
    }

    public List<Programacao> getProgramacao() {
        return ProjetoController.getInstance().listarProgramacao();
    }
}
