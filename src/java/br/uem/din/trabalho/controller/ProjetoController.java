package br.uem.din.trabalho.controller;

import br.uem.din.trabalho.model.Programacao;
import br.uem.din.trabalho.model.Projeto;
import java.util.ArrayList;
import java.util.List;

public class ProjetoController {

    private List<Projeto> projetos;
    private List<Projeto> projetoSelecionado;
    private static ProjetoController instance;

    private ProjetoController() {
        this.projetos = new ArrayList<>();
        this.projetoSelecionado = new ArrayList<>();
    }

    public static ProjetoController getInstance() {
        if (instance == null) {
            instance = new ProjetoController();
        }
        return instance;
    }

    public void salvarProjeto(Projeto projeto) {
        this.projetos.add(projeto);
    }

    public List<Projeto> listarProjetos() {
        return projetos;
    }

    public Projeto getProjeto(String ano, String nuProcesso) {
        List<Projeto> list = ProjetoController.getInstance().listarProjetos();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getAno().equals(ano) && list.get(i).getNuProcesso().equals(nuProcesso)) {
                return list.get(i);
            }
        }
        return null;
    }

    public void projetoSelecionado(String ano, String nuProcesso) {
        Projeto p = getProjeto(ano, nuProcesso);
        Projeto selecionado = new Projeto(p.getTitulo(), p.getNuProcesso(), p.getAno(), p.getProgramacao(), p.getParticipantes(), p.getBeneficiarios());
        this.projetoSelecionado.add(selecionado);
    }

    public void limparProjetoSelecionado() {
        this.projetoSelecionado.clear();
    }

    public List<Projeto> listarProjetoSelecionado() {
        return projetoSelecionado;
    }

    public List<Programacao> listarProgramacao() {
        return projetoSelecionado.get(0).getProgramacao();
    }
}
