package br.uem.din.trabalho.controller;

import br.uem.din.trabalho.model.Beneficiario;
import java.util.List;

public class BeneficiarioController {

    private static BeneficiarioController instance;

    private BeneficiarioController() {
    }

    public static BeneficiarioController getInstance() {
        if (instance == null) {
            instance = new BeneficiarioController();
        }
        return instance;
    }

    public void salvarBeneficiario(Beneficiario beneficiario) {
        ProjetoController.getInstance().listarProjetoSelecionado().get(0).getBeneficiarios().add(beneficiario);
    }

    public List<Beneficiario> listarBeneficiarios() {
        return ProjetoController.getInstance().listarProjetoSelecionado().get(0).getBeneficiarios();
    }

    public boolean deletarBeneficiario(String cpf) {
        boolean removed = false;
        List<Beneficiario> beneficiarios;
        beneficiarios = ProjetoController.getInstance().listarProjetoSelecionado().get(0).getBeneficiarios();
        for (int x = 0; x < beneficiarios.size(); x++) {
            if (beneficiarios.get(x).getCpf().equals(cpf)) {
                removed = beneficiarios.remove(beneficiarios.get(x));
            }
        }
        ProjetoController.getInstance().listarProjetoSelecionado().get(0).setBeneficiarios(beneficiarios);
        return removed;
    }

    public boolean validarBeneficiario(String cpf) {
        boolean existe = false;
        List<Beneficiario> beneficiarios;
        beneficiarios = ProjetoController.getInstance().listarProjetoSelecionado().get(0).getBeneficiarios();
        for (int x = 0; x < beneficiarios.size(); x++) {
            if (beneficiarios.get(x).getCpf().equals(cpf)) {
                existe = true;
            }
        }
        return existe;
    }
}
