package br.uem.din.trabalho.controller;

import br.uem.din.trabalho.model.Participante;
import java.util.List;

public class ParticipanteController {

    private static ParticipanteController instance;

    private ParticipanteController() {
    }

    public static ParticipanteController getInstance() {
        if (instance == null) {
            instance = new ParticipanteController();
        }
        return instance;
    }

    public void salvarParticipante(Participante participante) {
        ProjetoController.getInstance().listarProjetoSelecionado().get(0).getParticipantes().add(participante);
    }

    public List<Participante> listarPaticipantes() {
        return ProjetoController.getInstance().listarProjetoSelecionado().get(0).getParticipantes();
    }

    public boolean deletarParticipante(String cpf) {
        boolean removed = false;
        List<Participante> participantes;
        participantes = ProjetoController.getInstance().listarProjetoSelecionado().get(0).getParticipantes();

        for (int x = 0; x < participantes.size(); x++) {
            if (participantes.get(x).getCpf().equals(cpf)) {
                removed = participantes.remove(participantes.get(x));
            }
        }
        ProjetoController.getInstance().listarProjetoSelecionado().get(0).setParticipantes(participantes);
        return removed;
    }

    public boolean verificarParticipante(String cpf) {
        boolean existe = false;
        List<Participante> participantes;
        participantes = ProjetoController.getInstance().listarProjetoSelecionado().get(0).getParticipantes();
        for (int x = 0; x < participantes.size(); x++) {
            if (participantes.get(x).getCpf().equals(cpf)) {
                existe = true;
            }
        }
        return existe;
    }
}
