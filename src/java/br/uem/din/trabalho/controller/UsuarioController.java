package br.uem.din.trabalho.controller;

import br.uem.din.trabalho.model.Pessoa;

public class UsuarioController {

    private Pessoa pessoa;
    private static UsuarioController instance;

    private UsuarioController() {
        this.pessoa = new Pessoa();
    }

    public static UsuarioController getInstance() {
        if (instance == null) {
            instance = new UsuarioController();
        }
        return instance;
    }

    public void salvarUsuario(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Pessoa mostrarUsuario() {
        return pessoa;
    }
}
