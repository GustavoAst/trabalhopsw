package br.uem.din.trabalho.model;

import java.math.BigDecimal;

public class Pessoa {

    private String nome;
    private String cpf;
    private String matricula;
    private String email;
    private String internoUEM;
    private BigDecimal cargaHoraria;
    private Projeto projeto;

    public Pessoa(String nome, String cpf, String matricula, String email, String internoUEM, BigDecimal cargaHoraria, Projeto projeto) {
        this.nome = nome;
        this.cpf = cpf;
        this.matricula = matricula;
        this.email = email;
        this.internoUEM = internoUEM;
        this.cargaHoraria = cargaHoraria;
        this.projeto = projeto;
    }

    public Pessoa() {
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getInternoUEM() {
        return internoUEM;
    }

    public void setInternoUEM(String internoUEM) {
        this.internoUEM = internoUEM;
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public BigDecimal getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(BigDecimal cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

}
