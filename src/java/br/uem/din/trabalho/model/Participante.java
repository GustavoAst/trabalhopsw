package br.uem.din.trabalho.model;

import java.math.BigDecimal;

public class Participante extends Pessoa {

    private String curso;
    private String tipoAtuacao;

    public Participante(String curso, String tipoAtuacao, String nome, String cpf, String matricula, String email, String internoUEM, BigDecimal cargaHoraria, Projeto projeto) {
        super(nome, cpf, matricula, email, internoUEM, cargaHoraria, projeto);
        this.curso = curso;
        this.tipoAtuacao = tipoAtuacao;
    }

    public Participante() {
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getTipoAtuacao() {
        return tipoAtuacao;
    }

    public void setTipoAtuacao(String tipoAtuacao) {
        this.tipoAtuacao = tipoAtuacao;
    }

}
