package br.uem.din.trabalho.model;

public class Programacao {

    private String nomeTema;
    private int cargaHorariaTema;

    public Programacao(String nomeTema, int cargaHorariaTema) {
        this.nomeTema = nomeTema;
        this.cargaHorariaTema = cargaHorariaTema;
    }

    public Programacao() {
    }

    public String getNomeTema() {
        return nomeTema;
    }

    public void setNomeTema(String nomeTema) {
        this.nomeTema = nomeTema;
    }

    public int getCargaHorariaTema() {
        return cargaHorariaTema;
    }

    public void setCargaHorariaTema(int cargaHorariaTema) {
        this.cargaHorariaTema = cargaHorariaTema;
    }

}
