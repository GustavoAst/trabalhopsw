package br.uem.din.trabalho.model;

import java.math.BigDecimal;

public class Beneficiario extends Pessoa {

    private BigDecimal frequencia;
    private BigDecimal nota;

    public Beneficiario(BigDecimal frequencia, BigDecimal nota, String nome, String cpf, String matricula, String email, String internoUEM, BigDecimal cargaHoraria, Projeto projeto) {
        super(nome, cpf, matricula, email, internoUEM, cargaHoraria, projeto);
        this.frequencia = frequencia;
        this.nota = nota;
    }


    public Beneficiario() {
    }

    public BigDecimal getFrequencia() {
        return frequencia;
    }

    public void setFrequencia(BigDecimal frequencia) {
        this.frequencia = frequencia;
    }

    public BigDecimal getNota() {
        return nota;
    }

    public void setNota(BigDecimal nota) {
        this.nota = nota;
    }


}
