package br.uem.din.trabalho.model;

import java.util.List;

public class Projeto {

    private String titulo;
    private String nuProcesso;
    private String ano;
    private List<Programacao> programacao;
    private List<Participante> participantes;
    private List<Beneficiario> beneficiarios;

    public Projeto(String titulo, String nuProcesso, String ano, List<Programacao> programacao, List<Participante> participantes, List<Beneficiario> beneficiarios) {
        this.titulo = titulo;
        this.nuProcesso = nuProcesso;
        this.ano = ano;
        this.programacao = programacao;
        this.participantes = participantes;
        this.beneficiarios = beneficiarios;
    }

    public Projeto() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNuProcesso() {
        return nuProcesso;
    }

    public void setNuProcesso(String nuProcesso) {
        this.nuProcesso = nuProcesso;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public List<Programacao> getProgramacao() {
        return programacao;
    }

    public void setProgramacao(List<Programacao> programacao) {
        this.programacao = programacao;
    }

    public List<Participante> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(List<Participante> participantes) {
        this.participantes = participantes;
    }

    public List<Beneficiario> getBeneficiarios() {
        return beneficiarios;
    }

    public void setBeneficiarios(List<Beneficiario> beneficiarios) {
        this.beneficiarios = beneficiarios;
    }

}
