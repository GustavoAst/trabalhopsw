function TestaCPF(strCPF) {
    var Soma;
    var Resto;
    var i;
    Soma = 0;
    strCPF = strCPF.toString();

    if (strCPF.length < 11) {
        alert("O CPF precisa conter no minimo 11 digitos");
        return false;
    }
    if (strCPF === "00000000000") {
        alert("CPF invalido :" + strCPF);
        return false;
    }
    for (i = 1; i <= 9; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(9, 10))) {
        alert("CPF invalido :" + strCPF);
        return false;
    }
    Soma = 0;
    for (i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    }
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(10, 11))) {
        alert("CPF invalido :" + strCPF);
        return false;
    }
    return true;
}
function help() {
    alert("O Cpf precisa conter somente numeros");
}
function help1() {
    alert("A Frequencia deve conter somente numeros");
}
function help2() {
    alert("A nota nao e Obrigatoria");
}
function validaNota(NOTA) {
    var nota = parseInt(NOTA.toString());
    if (nota > 100) {
        alert("A nota deve ser no maximo 100");
    }
}
function validaFreq(FREQ) {
    var freq = parseInt(FREQ.toString());
    if (freq > 100) {
        alert("A frequencia deve ser no maximo 100");
    }
}